<?php
/**
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*/

$sql = array();

// $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'mtspayuapi` (
//     `id_mtspayuapi` int(11) NOT NULL AUTO_INCREMENT,
//     PRIMARY KEY  (`id_mtspayuapi`)
// ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'mtspayuapi`(
		`response_code` varchar(255) NOT NULL,
		`language_en` varchar(255) NOT NULL,
		`language_es` varchar(255) NOT NULL,
		`language_pt` varchar(255) NOT NULL
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = "
			INSERT INTO `"._DB_PREFIX_."mts_payu_rc` (`response_code`, `language_en`, `language_es`, `language_pt`) VALUES
			('ERROR', 'There was a general error.', 'Ocurrió un error general.', 'Ocorreu um erro geral.'),
			('APPROVED', 'The transaction was approved.', 'La transacción fue aprobada.', 'A transação foi aprovada.'),
			('ANTIFRAUD_REJECTED', 'The transaction was rejected by the anti-fraud system.', 'La transacción fue rechazada por el sistema anti-fraude.', 'A transação foi rejeitada pelo sistema anti fraude.'),
			('PAYMENT_NETWORK_REJECTED', 'The financial network rejected the transaction.', 'La red financiera rechazó la transacción.', 'A rede financeira rejeitou a transação.'),
			('ENTITY_DECLINED', 'The transaction was declined by the bank or financial network because of an error.', 'La transacción fue declinada por el banco o por la red financiera debido a un error.', 'A transação foi rejeitada pelo banco ou pela rede financeira devido a um erro.'),
			('INTERNAL_PAYMENT_PROVIDER_ERROR', 'An error occurred in the system trying to process the payment.', 'Ocurrió un error en el sistema intentando procesar el pago.', 'Ocorreu um erro no sistema tentando processar o pagamento.'),
			('INACTIVE_PAYMENT_PROVIDER', 'The payment provider was not active.', 'El proveedor de pagos no se encontraba activo.', 'O fornecedor de pagamentos não estava ativo.'),
			('DIGITAL_CERTIFICATE_NOT_FOUND', 'The financial network reported an authentication error.', 'La red financiera reportó un error en la autenticación.', 'A rede financeira relatou um erro na autenticação.'),
			('INVALID_EXPIRATION_DATE_OR_SECURITY_CODE', 'The security code or expiration date was invalid.', 'El código de seguridad o la fecha de expiración estaba inválido.', 'O código de segurança ou a data de expiração estava inválido.'),
			('INVALID_RESPONSE_PARTIAL_APPROVAL', 'Invalid response type. The entity response is a partial approval and should be automatically canceled by the system.', 'Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.', 'Tipo de resposta inválida. A entidade financeira aprovou parcialmente a transação e deve ser cancelado automaticamente pelo sistema.'),
			('INSUFFICIENT_FUNDS', 'The account had insufficient funds.', 'La cuenta no tenía fondos suficientes.', 'A conta não tinha crédito suficiente.'),
			('CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS', 'The credit card was not authorized for internet transactions.', 'La tarjeta de crédito no estaba autorizada para transacciones por Internet.', 'O cartão de crédito não estava autorizado para transações pela Internet.'),
			('INVALID_TRANSACTION', 'The financial network reported that the transaction was invalid.', 'La red financiera reportó que la transacción fue inválida.', 'A rede financeira relatou que a transação foi inválida.'),
			('INVALID_CARD', 'The card is invalid.', 'La tarjeta es inválida.', 'O cartão é inválido.'),
			('EXPIRED_CARD', 'The card has expired.', 'La tarjeta ya expiró.', 'O cartão já expirou.'),
			('RESTRICTED_CARD', 'The card has a restriction.', 'La tarjeta presenta una restricción.', 'O cartão apresenta uma restrição.'),
			('CONTACT_THE_ENTITY', 'You should contact the bank.', 'Debe contactar al banco.', 'Você deve entrar em contato com o banco.'),
			('REPEAT_TRANSACTION', 'You must repeat the transaction.', 'Se debe repetir la transacción.', 'Deve-se repetir a transação.'),
			('ENTITY_MESSAGING_ERROR', 'The financial network reported a communication error with the bank.', 'La red financiera reportó un error de comunicaciones con el banco.', 'A rede financeira relatou um erro de comunicações com o banco.'),
			('BANK_UNREACHABLE', 'The bank was not available.', 'El banco no se encontraba disponible.', 'O banco não se encontrava disponível.'),
			('EXCEEDED_AMOUNT', 'The transaction exceeds the amount set by the bank.', 'La transacción excede un monto establecido por el banco.', 'A transação excede um montante estabelecido pelo banco.'),
			('NOT_ACCEPTED_TRANSACTION', 'The transaction was not accepted by the bank for some reason.', 'La transacción no fue aceptada por el banco por algún motivo.', 'A transação não foi aceita pelo banco por algum motivo.'),
			('ERROR_CONVERTING_TRANSACTION_AMOUNTS', 'An error occurred converting the amounts to the payment currency.', 'Ocurrió un error convirtiendo los montos a la moneda de pago.', 'Ocorreu um erro convertendo os montantes para a moeda de pagamento.'),
			('EXPIRED_TRANSACTION', 'The transaction expired.', 'La transacción expiró.', 'A transação expirou.'),
			('PENDING_TRANSACTION_REVIEW', 'The transaction was stopped and must be revised, this can occur because of security filters.', 'La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.', 'A transação foi parada e deve ser revista, isto pode ocorrer por filtros de segurança.'),
			('PENDING_TRANSACTION_CONFIRMATION', 'The transaction is subject to confirmation.', 'La transacción está pendiente de ser confirmada.', 'A transação está pendente de confirmação.'),
			('PENDING_TRANSACTION_TRANSMISSION', 'The transaction is subject to be transmitted to the financial network. This usually applies to transactions with cash payment means.', 'La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.', 'A transação está pendente para ser transmitida para a rede financeira. Normalmente isto se aplica para transações com formas de pagamento em dinheiro.'),
			('PAYMENT_NETWORK_BAD_RESPONSE', 'The message returned by the financial network is inconsistent.', 'El mensaje retornado por la red financiera es inconsistente.', 'A mensagem retornada pela rede financeira é inconsistente.'),
			('PAYMENT_NETWORK_NO_CONNECTION', 'Could not connect to the financial network.', 'No se pudo realizar la conexión con la red financiera.', 'Não foi possível realizar a conexão com a rede financeira.'),
			('PAYMENT_NETWORK_NO_RESPONSE', 'Financial Network did not respond.', 'La red financiera no respondió.', 'A rede financeira não respondeu.'),
			('FIX_NOT_REQUIRED', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATIC_FIXED_NOT_SUPPORTED', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('NOT_FIXED_FOR_ERROR_STATE', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('ERROR_FIXING_AND_REVERSING', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('ERROR_FIXING_INCOMPLETE_DATA', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.');
";

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
