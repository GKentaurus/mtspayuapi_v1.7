<?php
/**
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*/

class MtspayuapiValidationPseModuleFrontController extends ModuleFrontController
{
    /**
     * This class should be use by your Instant Payment
     * Notification system to validate the order remotely
     */
    public function postProcess()
    {
        /**
         * If the module is not active anymore, no need to process anything.
         */
        if ($this->module->active == false) {
            die;
        }

        /**
         * Since it is an example, we choose sample data,
         * You'll have to get the correct values :)
         */

        $cart = $this->context->cart;

        $cart_id = $cart->id;
        $customer_id = $cart->id_customer;
        $amount = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $env_values = Tools::getAllValues();
        $currency = $this->context->currency;

        /**
         * Restore the context from the $cart_id & the $customer_id to process the validation properly.
         */
        Context::getContext()->cart = new Cart((int)$cart_id);
        Context::getContext()->customer = new Customer((int)$customer_id);
        Context::getContext()->currency = new Currency((int)Context::getContext()->cart->id_currency);
        Context::getContext()->language = new Language((int)Context::getContext()->customer->id_lang);

        $secure_key = Context::getContext()->customer->secure_key;

        $formVerification = $this->isValidForm($cart, $env_values);

        if (!$formVerification) {
            echo "<pre>";
            print_r($formVerification);
            echo "</pre>";
            die();
        }

        $payment_status = Configuration::get('PS_OS_MTS_PAYU_PENDING_CARD');
        $message = null;

        $module_name = $this->module->displayName;
        $currency_id = (int)Context::getContext()->currency->id;

        // $this->module->validateOrder($cart_id, $payment_status, $amount, $module_name, $message, array(), $currency_id, false, $secure_key);

        $temp_array = (array)$this->module;
        // $order_reference = $this->module->currentOrderReference;
        $order_reference = 'payment_test_000100003';

        $apiData = $this->apiBuilder($cart, $env_values, $order_reference, $currency->iso_code, $amount);

        if (!$apiData) {
            return false;
        }

        echo "<pre>";
        print_r($apiData);
        echo "</pre>";
        die();
    }

    protected function apiBuilder($cart, $env_values, $order_reference, $currency_iso_code, $amount)
    {
        $apiData = array();

        $apiData['language'] = Configuration::get('mts_payu_api_language');
        $apiData['command'] = 'SUBMIT_TRANSACTION';

        $apiData['merchant'] = array();
        $apiData['merchant']['apiKey'] = Configuration::get('mts_payu_api_key');
        $apiData['merchant']['apiLogin'] = Configuration::get('mts_payu_api_login');

        $apiData['transaction'] = array('order' => array());
        $apiData['transaction']['order']['accountId'] = Configuration::get('mts_payu_api_idaccount');
        $apiData['transaction']['order']['referenceCode'] = $order_reference;
        $apiData['transaction']['order']['description'] = 'Pago de la orden # ' . $order_reference;
        $apiData['transaction']['order']['language'] = Configuration::get('mts_payu_api_language');

        $signature_string = Configuration::get('mts_payu_api_key') .'~'. Configuration::get('mts_payu_api_idmerchant') .'~'. $order_reference .'~'. (float)$amount .'~'. $currency_iso_code;
        $payu_signature = md5($signature_string);

        $apiData['transaction']['order']['signature'] = $payu_signature;

        $url = $_SERVER['REQUEST_URI']; //returns the current URL
        $parts = explode('/', $url);
        $notifyUrl = _PS_BASE_URL_SSL_;
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $notifyUrl .= $parts[$i] . '/';
        }
        $notifyUrl .= 'notification';

        $apiData['transaction']['order']['notifyUrl'] = $notifyUrl;

        $apiData['transaction']['order']['additionalValues'] = array('TX_VALUE' => array());
        $apiData['transaction']['order']['additionalValues']['TX_VALUE']['value'] = $amount;
        $apiData['transaction']['order']['additionalValues']['TX_VALUE']['currency'] = $currency_iso_code;

        if ($cart->id_address_delivery == $cart->id_address_invoice) {
            $buyer = $this->dataContactBuilder($cart->id_address_delivery, $env_values);
            $payer = $buyer;
        } else {
            $buyer = $this->dataContactBuilder($cart->id_address_delivery, $env_values);
            $payer = $this->dataContactBuilder($cart->id_address_invoice, $env_values, 'payer');
        }

        if (!$buyer || !$payer) {
            return false;
        }

        $apiData['transaction']['order']['buyer'] = $buyer;
        $apiData['transaction']['order']['shippingAddress'] = $buyer['shippingAddress'];
        $apiData['transaction']['payer'] = $payer;

        $apiData['transaction']['creditCard'] = array();
        $apiData['transaction']['creditCard']['number'] = $env_values['cc_number'];
        $apiData['transaction']['creditCard']['securityCode'] = $env_values['cc_securityCode'];
        $apiData['transaction']['creditCard']['expirationDate'] = $env_values['cc_expDateYear'] . '/' . $env_values['cc_expDateMonth'];
        $apiData['transaction']['creditCard']['name'] = $env_values['cc_name'];

        $apiData['transaction']['extraParameters'] = array();
        $apiData['transaction']['extraParameters']['INSTALLMENTS_NUMBER'] = $env_values['cc_installments'];

        $apiData['transaction']['type'] = 'AUTHORIZATION_AND_CAPTURE';
        $apiData['transaction']['paymentMethod'] = $env_values['cc_franchise'];
        $apiData['transaction']['paymentCountry'] = Configuration::get('mts_payu_api_country');
        $apiData['transaction']['deviceSessionId'] = $env_values['deviceSessionId'];

        $ip_address = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ip_address = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip_address = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ip_address = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ip_address = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ip_address = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ip_address = getenv('REMOTE_ADDR');
        } else {
            $ip_address = 'UNKNOWN';
        }

        $apiData['transaction']['ipAddress'] = $ip_address;

        $apiData['transaction']['userAgent'] = $_SERVER['HTTP_USER_AGENT'];

        if ($env_values['cc_franchise'] == "CODENSA") {
            $apiData['transaction']['payer']['dniType'] = $env_values['cc_dnitype'];
        }

        $apiData['test'] = false;

        $jsonApiData = json_encode($apiData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $jsonData = json_decode($jsonApiData);

        if (Configuration::get('mts_payu_api_country') == true) {
            $api_url = 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi ';
        } else {
            $api_url = 'https://api.payulatam.com/payments-api/4.0/service.cgi';
        }

        $ch = curl_init($api_url);
        curl_setopt_array($ch, array(
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonApiData,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json')
        ));

        // JSON Format
        $jsonApiRequest = curl_exec($ch);

        //PHP Array Format
        $result = json_decode($jsonApiRequest, true);

        return $jsonApiData;
        return $result;
    }

    protected function isValidForm($cart, $env_values)
    {
        /**
         * Add your checks right there
         */

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || empty($cart->getProducts())) {
            return false;
        }

        // Datos de la tarjeta de crédito
        $cc_franchise = filter_var($env_values['cc_franchise'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);   //String
        $cc_name = filter_var($env_values['cc_name'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); //String
        $cc_dnitype = filter_var($env_values['cc_dnitype'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);   //String
        $cc_dni = filter_var($env_values['cc_dni'], FILTER_SANITIZE_NUMBER_INT); //Numeric
        $cc_number = filter_var($env_values['cc_number'], FILTER_SANITIZE_NUMBER_INT);   //Numeric
        $cc_securityCode = filter_var($env_values['cc_securityCode'], FILTER_SANITIZE_NUMBER_INT);   //Numeric
        $cc_installments = filter_var($env_values['cc_installments'], FILTER_SANITIZE_NUMBER_INT);   //Numeric
        $cc_expDateMonth = filter_var($env_values['cc_expDateMonth'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); //Numeric
        $cc_expDateYear = filter_var($env_values['cc_expDateYear'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);   //Numeric

        $cc_errors = array();

        if ($cc_franchise == false || $cc_franchise == 'invalid' || $cc_franchise == '') {
            $cc_errors['cc_franchise'] = 'Failed';
        } else {
            $cc_errors['cc_franchise'] = 'Ok';
        }

        if ($cc_name == false || $cc_name == '') {
            $cc_errors['cc_name'] = 'Failed';
        } else {
            $cc_errors['cc_name'] = 'Ok';
        }

        if ($cc_dnitype == false || $cc_dnitype == 'invalid' || $cc_dnitype == '') {
            $cc_errors['cc_dnitype'] = 'Failed';
        } else {
            $cc_errors['cc_dnitype'] = 'Ok';
        }

        if ($cc_dni == false || $cc_dni == '') {
            $cc_errors['cc_dni'] = 'Failed';
        } else {
            $cc_errors['cc_dni'] = 'Ok';
        }

        if ($cc_number == false || $cc_number == '') {
            $cc_errors['cc_number'] = 'Failed';
        } else {
            $cc_errors['cc_number'] = 'Ok';
        }

        if ($cc_securityCode == false || $cc_securityCode == '') {
            $cc_errors['cc_securityCode'] = 'Failed';
        } else {
            $cc_errors['cc_securityCode'] = 'Ok';
        }

        if ($cc_installments == false || $cc_installments == 'invalid' || $cc_installments == '') {
            $cc_errors['cc_installments'] = 'Failed';
        } else {
            $cc_errors['cc_installments'] = 'Ok';
        }

        if ($cc_expDateMonth == false || $cc_expDateMonth == '' || $cc_expDateMonth == 'invalid') {
            $cc_errors['cc_expDateMonth'] = 'Failed';
        } else {
            $cc_errors['cc_expDateMonth'] = 'Ok';
        }

        if ($cc_expDateYear == false || $cc_expDateYear == '' || $cc_expDateYear == 'invalid') {
            $cc_errors['cc_expDateYear'] = 'Failed';
        } else {
            $cc_errors['cc_expDateYear'] = 'Ok';
        }

        if ($cc_errors['cc_expDateYear'] == 'Ok' && $cc_errors['cc_expDateMonth'] == 'Ok' && (($cc_expDateYear >= date('Y') && $cc_expDateMonth >= date('n')) || ($cc_expDateYear > date('Y')))) {
            $cc_errors['cc_expDate'] = 'Ok';
        } else {
            $cc_errors['cc_expDate'] = 'Failed';
        }

        if (array_search('Failed', $cc_errors)) {
            return $cc_errors;
        } else {
            return true;
        }
    }

    /**
     * Contact Data compiler
     */
    protected function dataContactBuilder($address_type, $env_values, $type = 'buyer')
    {
        //Address Array
        $sql = new DbQuery();
        $sql->select('*')
            ->from('address')
            ->where('id_address = '.$address_type)
            ->orderBy('id_address');
        $address = Db::getInstance()->executeS($sql);

        //State Array
        $sql = new DbQuery();
        $sql->select('*')
            ->from('state')
            ->where('id_state = '.$address[0]['id_state'])
            ->orderBy('id_state');
        $state = Db::getInstance()->executeS($sql);

       //Invoice Country Array
        $sql = new DbQuery();
        $sql->select('*')
            ->from('country')
            ->where('id_country = '.$address[0]['id_country'])
            ->orderBy('id_country');
        $country = Db::getInstance()->executeS($sql);

        $contact = array();

        $contact['fullName'] = $address[0]['firstname'] . ' ' . $address[0]['lastname'];
        $contact['emailAddress'] = $this->context->customer->email;

        if ($address[0]['phone'] != null || $address[0]['phone'] != '') {
            $contact['contactPhone'] = $address[0]['phone'];
        } elseif ($address[0]['phone_mobile'] != null || $address[0]['phone_mobile'] != '') {
            $contact['contactPhone'] = $address[0]['phone_mobile'];
        } else {
            $contact['contactPhone'] = '';
        }

        $contact['dniNumber'] = $env_values['cc_dni'];

        if ($type == 'buyer') {
            $mailingAddress = 'shippingAddress';
        } else {
            $mailingAddress = 'billingAddress';
        }

        $contact[$mailingAddress] = array();

        $contact[$mailingAddress]['street1'] = $address[0]['address1'];
        $contact[$mailingAddress]['street2'] = $address[0]['address2'];
        $contact[$mailingAddress]['city'] = $address[0]['city'];
        $contact[$mailingAddress]['state'] = $state[0]['name'];
        $contact[$mailingAddress]['country'] = $country[0]['iso_code'];
        $contact[$mailingAddress]['postalCode'] = $address[0]['postcode'];

        if ($address[0]['phone'] != null || $address[0]['phone'] != '') {
            $contact[$mailingAddress]['phone'] = $address[0]['phone'];
        } elseif ($address[0]['phone_mobile'] != null || $address[0]['phone_mobile'] != '') {
            $contact[$mailingAddress]['phone'] = $address[0]['phone_mobile'];
        } else {
            $contact[$mailingAddress]['phone'] = '';
        }

        return $contact;
    }
}
