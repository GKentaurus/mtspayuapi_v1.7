{*
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*}

<div class="panel">
	<div class="row mtspayuapi-header">
		<img src="{$module_dir|escape:'html':'UTF-8'}views/img/template_2_logo.png" class="col-xs-6 col-md-3 text-center" id="payment-logo" />
		<div class="col-xs-6 col-md-6 text-center text-muted">
			{l s='My Payment Module and PrestaShop have partnered to provide the easiest way for you to accurately calculate and file sales tax.' mod='mtspayuapi'}
		</div>
		<div class="col-xs-12 col-md-3 text-center">
			<a href="#" onclick="javascript:return false;" class="btn btn-primary" id="create-account-btn">{l s='Create an account' mod='mtspayuapi'}</a><br />
			{l s='Already have one?' mod='mtspayuapi'}<a href="#" onclick="javascript:return false;"> {l s='Log in' mod='mtspayuapi'}</a>
		</div>
	</div>

	<hr />
	
	<div class="mtspayuapi-content">
		<div class="row">
			<div class="col-md-5">
				<h5>{l s='Benefits of using my payment module' mod='mtspayuapi'}</h5>
				<ul class="ul-spaced">
					<li>
						<strong>{l s='It is fast and easy' mod='mtspayuapi'}:</strong>
						{l s='It is pre-integrated with PrestaShop, so you can configure it with a few clicks.' mod='mtspayuapi'}
					</li>
					
					<li>
						<strong>{l s='It is global' mod='mtspayuapi'}:</strong>
						{l s='Accept payments in XX currencies from XXX markets around the world.' mod='mtspayuapi'}
					</li>
					
					<li>
						<strong>{l s='It is trusted' mod='mtspayuapi'}:</strong>
						{l s='Industry-leading fraud an buyer protections keep you and your customers safe.' mod='mtspayuapi'}
					</li>
					
					<li>
						<strong>{l s='It is cost-effective' mod='mtspayuapi'}:</strong>
						{l s='There are no setup fees or long-term contracts. You only pay a low transaction fee.' mod='mtspayuapi'}
					</li>
				</ul>
			</div>
			
			<div class="col-md-2">
				<h5>{l s='Pricing' mod='mtspayuapi'}</h5>
				<dl class="list-unstyled">
					<dt>{l s='Payment Standard' mod='mtspayuapi'}</dt>
					<dd>{l s='No monthly fee' mod='mtspayuapi'}</dd>
					<dt>{l s='Payment Express' mod='mtspayuapi'}</dt>
					<dd>{l s='No monthly fee' mod='mtspayuapi'}</dd>
					<dt>{l s='Payment Pro' mod='mtspayuapi'}</dt>
					<dd>{l s='$5 per month' mod='mtspayuapi'}</dd>
				</dl>
				<a href="#" onclick="javascript:return false;">(Detailed pricing here)</a>
			</div>
			
			<div class="col-md-5">
				<h5>{l s='How does it work?' mod='mtspayuapi'}</h5>
				<iframe src="//player.vimeo.com/video/75405291" width="335" height="188" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
		</div>

		<hr />
		
		<div class="row">
			<div class="col-md-12">
				<p class="text-muted">{l s='My Payment Module accepts more than 80 localized payment methods around the world' mod='mtspayuapi'}</p>
				
				<div class="row">
					<img src="{$module_dir|escape:'html':'UTF-8'}views/img/template_2_cards.png" class="col-md-3" id="payment-logo" />
					<div class="col-md-9 text-center">
						<h6>{l s='For more information, call 888-888-1234' mod='mtspayuapi'} {l s='or' mod='mtspayuapi'} <a href="mailto:contact@prestashop.com">contact@prestashop.com</a></h6>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="panel">
	<p class="text-muted">
		<i class="icon icon-info-circle"></i> {l s='In order to create a secure account with My Payment Module, please complete the fields in the settings panel below:' mod='mtspayuapi'}
		{l s='By clicking the "Save" button you are creating secure connection details to your store.' mod='mtspayuapi'}
		{l s='My Payment Module signup only begins when you client on "Activate your account" in the registration panel below.' mod='mtspayuapi'}
		{l s='If you already have an account you can create a new shop within your account.' mod='mtspayuapi'}
	</p>
	<p>
		<a href="#" onclick="javascript:return false;"><i class="icon icon-file"></i> Link to the documentation</a>
	</p>
</div>