{*
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*}

<div class="panel">
	<div class="row mtspayuapi-header">
		<img src="{$module_dir|escape:'html':'UTF-8'}views/img/template_1_logo.png" class="col-xs-6 col-md-4 text-center" id="payment-logo" />
		<div class="col-xs-6 col-md-4 text-center">
			<h4>{l s='Online payment processing' mod='mtspayuapi'}</h4>
			<h4>{l s='Fast - Secure - Reliable' mod='mtspayuapi'}</h4>
		</div>
		<div class="col-xs-12 col-md-4 text-center">
			<a href="#" onclick="javascript:return false;" class="btn btn-primary" id="create-account-btn">{l s='Create an account now!' mod='mtspayuapi'}</a><br />
			{l s='Already have an account?' mod='mtspayuapi'}<a href="#" onclick="javascript:return false;"> {l s='Log in' mod='mtspayuapi'}</a>
		</div>
	</div>

	<hr />
	
	<div class="mtspayuapi-content">
		<div class="row">
			<div class="col-md-6">
				<h5>{l s='My payment module offers the following benefits' mod='mtspayuapi'}</h5>
				<dl>
					<dt>&middot; {l s='Increase customer payment options' mod='mtspayuapi'}</dt>
					<dd>{l s='Visa®, Mastercard®, Diners Club®, American Express®, Discover®, Network and CJB®, plus debit, gift cards and more.' mod='mtspayuapi'}</dd>
					
					<dt>&middot; {l s='Help to improve cash flow' mod='mtspayuapi'}</dt>
					<dd>{l s='Receive funds quickly from the bank of your choice.' mod='mtspayuapi'}</dd>
					
					<dt>&middot; {l s='Enhanced security' mod='mtspayuapi'}</dt>
					<dd>{l s='Multiple firewalls, encryption protocols and fraud protection.' mod='mtspayuapi'}</dd>
					
					<dt>&middot; {l s='One-source solution' mod='mtspayuapi'}</dt>
					<dd>{l s='Conveniance of one invoice, one set of reports and one 24/7 customer service contact.' mod='mtspayuapi'}</dd>
				</dl>
			</div>
			
			<div class="col-md-6">
				<h5>{l s='FREE My Payment Module Glocal Gateway (Value of 400$)' mod='mtspayuapi'}</h5>
				<ul>
					<li>{l s='Simple, secure and reliable solution to process online payments' mod='mtspayuapi'}</li>
					<li>{l s='Virtual terminal' mod='mtspayuapi'}</li>
					<li>{l s='Reccuring billing' mod='mtspayuapi'}</li>
					<li>{l s='24/7/365 customer support' mod='mtspayuapi'}</li>
					<li>{l s='Ability to perform full or patial refunds' mod='mtspayuapi'}</li>
				</ul>
				<br />
				<em class="text-muted small">
					* {l s='New merchant account required and subject to credit card approval.' mod='mtspayuapi'}
					{l s='The free My Payment Module Global Gateway will be accessed through log in information provided via email within 48 hours.' mod='mtspayuapi'}
					{l s='Monthly fees for My Payment Module Global Gateway will apply.' mod='mtspayuapi'}
				</em>
			</div>
		</div>

		<hr />
		
		<div class="row">
			<div class="col-md-12">
				<h4>{l s='Accept payments in the United States using all major credit cards' mod='mtspayuapi'}</h4>
				
				<div class="row">
					<img src="{$module_dir|escape:'html':'UTF-8'}views/img/template_1_cards.png" class="col-md-6" id="payment-logo" />
					<div class="col-md-6">
						<h6 class="text-branded">{l s='For transactions in US Dollars (USD) only' mod='mtspayuapi'}</h6>
						<p class="text-branded">{l s='Call 888-888-1234 if you have any questions or need more information!' mod='mtspayuapi'}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>