{*
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*}

<form id="form_cc" class="m-top-negativo" role="form" method="post" action="{$action}">
  <p style="background:url(https://maf.pagosonline.net/ws/fp?id={$deviceSessionId})"></p>
  <img src="https://maf.pagosonline.net/ws/fp/clear.png?id={$deviceSessionId}" class="img-none">
  <script src="https://maf.pagosonline.net/ws/fp/check.js?id={$deviceSessionId}"></script>
  <object type="application/x-shockwave-flash" data="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" width="1" height="1" id="thm_fp">
    <param name="movie" value="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" />
  </object>
  <div class="cc-moneda">
    <p class="m-top-negativo">
      {if isset($currencies) && $currencies|@count > 1}
        {l s='Aceptamos varias monedas para este método de pago' mod='mtspayuapi'}
    </p>
    <p>
      {l s='Elija una de las siguientes' mod='mtspayuapi'}
      <select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
        {foreach from=$currencies item=optcurrency}
          <option value="{$optcurrency.id_currency}" {if isset($currencies) && $optcurrency.id_currency == $currency->id} selected="selected"{/if}> {$optcurrency.sign} {$optcurrency.name} ({$optcurrency.iso_code})</option>
        {/foreach}
      </select>
      {else}
        {l s='Solo aceptamos las siguientes monedas para este metodo de pago:' mod='mtspayuapi'}&nbsp;<b>{$currencies.0.name}</b>
        <input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
      {/if}
    </p>
  </div>

  <!-- <h4 class="mts-text-c cc-font-size">{l s='Por favor ingresa los datos de tu tarjeta de crédito o débito:' mod='mtspayuapi'}</h4> -->
  
  <input type="hidden" id="deviceSessionId" name="deviceSessionId" value="{$deviceSessionId}"/>
  
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
        <span>{l s='Seleccione la franquicia:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6">
      <select name="cc_franchise" id="cc_franchise">
        <option value="invalid">{l s='- Seleccione una opción -' mod='mtspayuapi'}</option>
        <option value="VISA">{l s='VISA' mod='mtspayuapi'}</option>
        <option value="MASTERCARD">{l s='MASTER CARD' mod='mtspayuapi'}</option>
        <option value="AMEX">{l s='AMERICAN EXPRESS' mod='mtspayuapi'}</option>
        <option value="DINERS">{l s='DINERS CLUB' mod='mtspayuapi'}</option>
        <option value="CODENSA">{l s='CODENSA' mod='mtspayuapi'}</option>
      </select>
    </div>
  </div>
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
      <span>{l s='Nombre del propietario:' mod='mtspayuapi'} </span> 
    </div>
    <div class="col col-xs-12 col-sm-6">
      <input name="cc_name" id="cc_name"/>
    </div>
  </div>
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
      <span>{l s='Documento de identificación:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6 pre-duo">
      <div class="duo">
        <select name="cc_dnitype" id="cc_dnitype">
          <option value="invalid">{l s='--' mod='mtspayuapi'}</option>
          <option value="CC">{l s='CC' mod='mtspayuapi'}</option>
          <option value="CE">{l s='CE' mod='mtspayuapi'}</option>
          <option value="NIT">{l s='NIT' mod='mtspayuapi'}</option>
          <option value="TI">{l s='TI' mod='mtspayuapi'}</option>
          <option value="PP">{l s='PP' mod='mtspayuapi'}</option>
          <option value="IDC">{l s='IDC' mod='mtspayuapi'}</option>
          <option value="CEL">{l s='CEL' mod='mtspayuapi'}</option>
          <option value="RC">{l s='RC' mod='mtspayuapi'}</option>
          <option value="DE">{l s='DE' mod='mtspayuapi'}</option>
        </select>
        <input name="cc_dni" id="cc_dni" type="number"/>
      </div>
    </div>
  </div>
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
      <span>{l s='Número de tarjeta:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6">
    {literal}
      <input name="cc_number" id="cc_number" type="number"/>
    {/literal}
    </div>
  </div>
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
        <span>{l s='Código de seguridad:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6">
      {literal} 
        <input name="cc_securityCode" id="cc_securityCode" type="number"/>
      {/literal} 
      <!-- Info secutiry code -->
      <div id="cc_showinfo_sc">
        <a href="#cc_info_securitycode">CVV/CVC</a><i class="fa fa-info-circle" aria-hidden="true"></i>
      </div>
      <div id="cc_info_securitycode">
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <h3>
              <i class="fa fa-info-circle info" aria-hidden="true"></i>
              <span> Código de seguridad </span>
            </h3>
          </div>
          <div class="col-xs-12 col-sm-7">
            <img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/cvv-cards.png" alt="3 Digitos">
          </div>
          <div class="col-xs-12 col-sm-5">
            <p>
              Código de seguridad de 3 dígitos
            </p>
          </div>
          <div class="col-xs-12 col-sm-7">
            <img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/cvv-amex.png" alt="4 Digitos">
          </div>
          <div class="col-xs-12 col-sm-5">
            <p>
              Código de seguridad de 4 dígitos
            </p>
          </div>
        </div>
      </div>
      <!-- Fin info secutiry code -->
    </div> 
  </div>
  <div class="payment_info">
    <div class="col col-xs-12 col-sm-6">
        <span>{l s='Número de cuotas:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6">
      <select name="cc_installments" id="cc_installments" />
        <option value="invalid"> -- </option>
      </select>
    </div>
  </div>
  <div class="payment_info p_fin">
    <div class="col col-xs-12 col-sm-6">
        <span>{l s='Fecha de vencimiento:' mod='mtspayuapi'}</span>
    </div>
    <div class="col col-xs-12 col-sm-6 pre-duo">
      <div class="duo">
        <select name="cc_expDateMonth" id="cc_expDateMonth">
          <option value="invalid">{l s='- Elija un mes -' mod='mtspayuapi'}</option>
          <option value="01">(01) {l s='Enero' mod='mtspayuapi'}</option>
          <option value="02">(02) {l s='Febrero' mod='mtspayuapi'}</option>
          <option value="03">(03) {l s='Marzo' mod='mtspayuapi'}</option>
          <option value="04">(04) {l s='Abril' mod='mtspayuapi'}</option>
          <option value="05">(05) {l s='Mayo' mod='mtspayuapi'}</option>
          <option value="06">(06) {l s='Junio' mod='mtspayuapi'}</option>
          <option value="07">(07) {l s='Julio' mod='mtspayuapi'}</option>
          <option value="08">(08) {l s='Agosto' mod='mtspayuapi'}</option>
          <option value="09">(09) {l s='Septiembre' mod='mtspayuapi'}</option>
          <option value="10">(10) {l s='Octubre' mod='mtspayuapi'}</option>
          <option value="11">(11) {l s='Noviembre' mod='mtspayuapi'}</option>
          <option value="12">(12) {l s='Diciembre' mod='mtspayuapi'}</option>
        </select>
        {html_select_date prefix="cc_expDate" time=$time start_year="+0" end_year="+15" display_days=false display_months=false year_extra='id="cc_expDateYear"'}
      </div>
    </div>
  </div>
  
  <div id="goddady-ssl-certified"> 
    <p> La transacción está protegida por: </p>
    <a href="https://seal.godaddy.com/verifySeal?sealID=E7OfHfcEYKhAunZbXlO1VdAUeD0R0Atcit4pseBuMxQrA5mXB8z8CaMue8v3" target="_blank" style="display: inline;text-align: center;margin-bottom: 5px;"> 
      <img src="https://seal.godaddy.com/images/3/es/siteseal_gd_3_h_l_m.gif" alt="Sitio Seguro" title="Sitio Seguro" style="width: 175px;height: auto;"> 
    </a>
    <img style="width: 90px; margin-left:5px;" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/logopayu.png" alt="Codensa">
  </div>
  
  <p style="font-size:12px;">
    {l s='*Nosotros no almacenamos ningún tipo de información sobre su tarjeta crédito o débito.*' mod='mtspayuapi'}
  </p>

  
</form>