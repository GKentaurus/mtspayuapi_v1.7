/**
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*/

// console.log('MTS JS Cargado en esta página');
$(document).ready(function () {

    // $("a#cc_showinfo_sc").fancybox();

    // var pathUrl = window.location.pathname;
    // if (pathUrl.search('mtspayuapi') >= 0)
    // {
    // 	$('#left_column').hide();
    // }

    /*	Tarjeta Crédito / Débito 	*/
    $('#cc_franchise').change(function () {
        if (this.value == "CODENSA") {
            $('#cc_installments').empty();
            $('#cc_installments').append('<option value="invalid"> -- </option>');
            $('#cc_installments').append('<option value="' + 6 + '">' + 6 + '</option>');
            $('#cc_installments').append('<option value="' + 12 + '">' + 12 + '</option>');
            $('#cc_installments').append('<option value="' + 18 + '">' + 18 + '</option>');
            $('#cc_installments').append('<option value="' + 24 + '">' + 24 + '</option>');
            $('#cc_installments').append('<option value="' + 36 + '">' + 36 + '</option>');
            $('#cc_installments').append('<option value="' + 46 + '">' + 46 + '</option>');
        } else if (this.value != 'invalid') {
            $('#cc_installments').empty();
            $('#cc_installments').append('<option value="invalid"> -- </option>');
            for (var i = 1; i <= 36; i++) {
                $('#cc_installments').append('<option value="' + i + '">' + i + '</option>');
            }
        } else {
            $('#cc_installments').empty();
            $('#cc_installments').append('<option value="invalid"> -- </option>');
        }

        if ($(this).val() == 'AMEX') {
            if ( $('#cc_securityCode').val().length > 4) {
                var value = $('#cc_securityCode').val();
                value = value.substr(0,4);
                $('#cc_securityCode').val(value);
            }
        } else {
            if ( $('#cc_securityCode').val().length > 3) {
                var value = $('#cc_securityCode').val();
                value = value.substr(0,3);
                $('#cc_securityCode').val(value);
            }
        }
    });

    $('#sendCard').click(function (e) {
        e.preventDefault();
        var form = $('#form_cc').serialize();
        $.ajax({
            method: 'POST',
            url: 'card_validate',
            data: form,
            dataType: 'json'
        }).done(function (info) {
            if (info != null || info != undefined) {
                if (info['redirect'] != undefined) {
                    redirection(info['redirect']);
                }
                cc_showErrors(info);
            } else {
                $('#payment_error').fadeOut('slow');
                $('#form_cc').submit();
            }
            
        })
    });

    function cc_showErrors(info)
    {
        if (
            info['cc_franchise'] == "Failed" ||
            info['cc_name'] == "Failed" ||
            info['cc_dnitype'] == "Failed" ||
            info['cc_dni'] == "Failed" ||
            info['cc_number'] == "Failed" ||
            info['cc_securityCode'] == "Failed" ||
            info['cc_installments'] == "Failed" ||
            info['cc_expDateMonth'] == "Failed" ||
            info['cc_expDateYear'] == "Failed"
        ) {
            $('#payment_error').fadeIn('slow');
        } else {
            $('#payment_error').fadeOut('slow');
        }

        if (info['cc_franchise'] == "Failed") {
            $('#error_franchise').fadeIn('slow');
        } else if (info['cc_franchise'] == "Ok") {
            $('#error_franchise').fadeOut('slow');
        }

        if (info['cc_name'] == "Failed") {
            $('#error_name').fadeIn('slow');
        } else if (info['cc_name'] == "Ok") {
            $('#error_name').fadeOut('slow');
        }

        if (info['cc_dnitype'] == "Failed") {
            $('#error_dnitype').fadeIn('slow');
        } else if (info['cc_dnitype'] == "Ok") {
            $('#error_dnitype').fadeOut('slow');
        }

        if (info['cc_dni'] == "Failed") {
            $('#error_dni').fadeIn('slow');
        } else if (info['cc_dni'] == "Ok") {
            $('#error_dni').fadeOut('slow');
        }

        if (info['cc_number'] == "Failed") {
            $('#error_number').fadeIn('slow');
        } else if (info['cc_number'] == "Ok") {
            $('#error_number').fadeOut('slow');
        }

        if (info['cc_securityCode'] == "Failed") {
            $('#error_securityCode').fadeIn('slow');
        } else if (info['cc_securityCode'] == "Ok") {
            $('#error_securityCode').fadeOut('slow');
        }

        if (info['cc_installments'] == "Failed") {
            $('#error_installments').fadeIn('slow');
        } else if (info['cc_installments'] == "Ok") {
            $('#error_installments').fadeOut('slow');
        }

        if (info['cc_expDateMonth'] == "Failed" || info['cc_expDateYear'] == "Failed" ) {
            $('#error_expDate').fadeIn('slow');
        } else if (info['cc_expDateMonth'] == "Ok" && info['cc_expDateYear'] == "Ok") {
            $('#error_expDate').fadeOut('slow');
        }

        $('html, body').animate({
            scrollTop: $("#payment_error").offset().top
        }, 1000);
    };

    $('#cc_name').keyup(function () {
        var value = $(this).val();
        var charVal = value.substr(value.length-1,1);

        for (var i = 0; i <= 9; i++) {
            if ( parseInt(charVal) == i) {
                $(this).val(value.substr(0, value.length-1));
            }
        }
    }).focusout(function () {
        var value = $(this).val();
        for (var i = value.length - 1; i >= 0; i--) {
            for (var k = 0; k <= 9; k++) {
                value = value.replace(k,'');
            }
        }
        $(this).val(value);
    });

    $('#cc_number').keyup(function () {
        if ( $(this).val().length > 16) {
            var value = $(this).val();
            value = value.substr(0,16);
            $(this).val(value);
        }
    });

    $('#cc_securityCode').keyup(function () {
        if ($('#cc_franchise').val() == 'AMEX') {
            if ( $(this).val().length > 4) {
                var value = $(this).val();
                value = value.substr(0,4);
                $(this).val(value);
            }
        } else {
            if ( $(this).val().length > 3) {
                var value = $(this).val();
                value = value.substr(0,3);
                $(this).val(value);
            }
        }
    });

    /* Pagos en Línea (PSE) */
    $('#sendPSE').click(function (e) {
        e.preventDefault();
        var form = $('#form_pse').serialize();
        $.ajax({
            method: 'POST',
            url: 'pse_validate',
            data: form,
            dataType: 'json'
        }).done(function (info) {
            if (info != null || info != undefined) {
                if (info['redirect'] != undefined) {
                    redirection(info['redirect']);
                }
                pse_showErrors(info);
            } else {
                $('#payment_error').fadeOut('slow');
                console.log(info);
                console.log('Todo bien');
                $('#form_pse').submit();
            }
            
        })
    });

    function pse_showErrors(info)
    {
        if (
            info['pse_bank'] == "Failed" ||
            info['pse_name'] == "Failed" ||
            info['pse_type'] == "Failed" ||
            info['pse_dnitype'] == "Failed" ||
            info['pse_dni'] == "Failed" ||
            info['pse_phone'] == "Failed"
        ) {
            $('#payment_error').fadeIn('slow');
        } else {
            $('#payment_error').fadeOut('slow');
        }

        if (info['pse_bank'] == "Failed") {
            $('#error_bank').fadeIn('slow');
        } else if (info['pse_bank'] == "Ok") {
            $('#error_bank').fadeOut('slow');
        }

        if (info['pse_name'] == "Failed") {
            $('#error_name').fadeIn('slow');
        } else if (info['pse_name'] == "Ok") {
            $('#error_name').fadeOut('slow');
        }

        if (info['pse_type'] == "Failed") {
            $('#error_type').fadeIn('slow');
        } else if (info['pse_type'] == "Ok") {
            $('#error_type').fadeOut('slow');
        }

        if (info['pse_dnitype'] == "Failed") {
            $('#error_dnitype').fadeIn('slow');
        } else if (info['pse_dnitype'] == "Ok") {
            $('#error_dnitype').fadeOut('slow');
        }

        if (info['pse_dni'] == "Failed") {
            $('#error_dni').fadeIn('slow');
        } else if (info['pse_dni'] == "Ok") {
            $('#error_dni').fadeOut('slow');
        }

        if (info['pse_phone'] == "Failed") {
            $('#error_number').fadeIn('slow');
        } else if (info['pse_phone'] == "Ok") {
            $('#error_number').fadeOut('slow');
        }

        $('html, body').animate({
            scrollTop: $("#payment_error").offset().top
        }, 1000);
    };

    function redirection(url_getted)
    {
        var basedir = $('#basedir').val();
        window.location.replace(basedir + url_getted);
    }
});
