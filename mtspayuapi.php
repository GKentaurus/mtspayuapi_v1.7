<?php
/**
* 2017 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2017 Metasysco S.A.S.
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* @version 1.2.0
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

class Mtspayuapi extends PaymentModule
{
    protected $config_form = false;
    private $_postErrors = array();

    public function __construct()
    {
        $this->name = 'mtspayuapi';
        $this->tab = 'payments_gateways';
        $this->version = '1.2.0';
        $this->author = 'Metasysco S.A.S.';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Metasysco.com - Pagos a través de PayU');
        $this->description = $this->l('Plataforma de pago basada en la API de PayU, desarrollada por Metasysco S.A.S., en la que su cliente siempre retornará a su página de comercio para realizar el pago. De esta forma podrá hacer siguimiento de las compras realizadas a través de este método de pago. Solo Latinoamérica.');

        $this->confirmUninstall = $this->l('¿Está seguro que desea desinstalar? Se borrará toda la información relacionada con la API de PayU.');

        $this->limited_countries = array('AR', 'BR', 'CO', 'MX', 'PA', 'PE');

        // $this->limited_currencies = array('EUR');

        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => '1.7.2.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */


    public function install()
    {
        if (extension_loaded('curl') == false) {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        if (in_array($iso_code, $this->limited_countries) == false) {
            $this->_errors[] = $this->l('Este módulo no está disponible en tu país.');
            return false;
        }

        Configuration::updateValue('mts_payu_sandbox_mode', true);
        Configuration::updateValue('mts_payu_company_name', 'Company Name');
        Configuration::updateValue('mts_payu_company_nit', '99999999999-9');
        Configuration::updateValue('mts_payu_api_key', '4Vj8eK4rloUd272L48hsrarnUA');
        Configuration::updateValue('mts_payu_api_login', 'pRRXKOl8ikMmt9u');
        Configuration::updateValue('mts_payu_api_publickey', '0000000000000');
        Configuration::updateValue('mts_payu_api_idmerchant', '512321');
        Configuration::updateValue('mts_payu_api_idaccount', '512321');
        Configuration::updateValue('mts_payu_api_country', 'CO');
        Configuration::updateValue('mts_payu_api_language', 'es');
        Configuration::updateValue('mts_payu_extrainfo_card_mode', false);
        Configuration::updateValue('mts_payu_extrainfo_card_link', '');
        Configuration::updateValue('mts_payu_extrainfo_pse_mode', false);
        Configuration::updateValue('mts_payu_extrainfo_pse_link', '');
        Configuration::updateValue('mts_payu_extrainfo_efecty_mode', false);
        Configuration::updateValue('mts_payu_extrainfo_efecty_link', '');
        Configuration::updateValue('mts_payu_extrainfo_baloto_mode', false);
        Configuration::updateValue('mts_payu_extrainfo_baloto_link', '');
        Configuration::updateValue('mts_payu_extrainfo_othercash_mode', false);
        Configuration::updateValue('mts_payu_extrainfo_othercash_link', '');

        include(dirname(__FILE__).'/sql/install.php');

        if (!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('backOfficeHeader') ||
            !$this->registerHook('payment') ||
            !$this->registerHook('paymentReturn') ||
            !$this->registerHook('actionPaymentCCAdd') ||
            !$this->registerHook('actionPaymentConfirmation') ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayPayment') ||
            !$this->registerHook('paymentOptions') ||
            !$this->registerHook('displayPaymentReturn')
            ) {
            return false;
        }

        if (!$this->installOrderState()) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        Configuration::deleteByName('mts_payu_sandbox_mode');
        Configuration::deleteByName('mts_payu_company_name');
        Configuration::deleteByName('mts_payu_company_nit');
        Configuration::deleteByName('mts_payu_api_key');
        Configuration::deleteByName('mts_payu_api_login');
        Configuration::deleteByName('mts_payu_api_publickey');
        Configuration::deleteByName('mts_payu_api_idmerchant');
        Configuration::deleteByName('mts_payu_api_idaccount');
        Configuration::deleteByName('mts_payu_api_country');
        Configuration::deleteByName('mts_payu_api_language');
        Configuration::deleteByName('mts_payu_extrainfo_card_mode');
        Configuration::deleteByName('mts_payu_extrainfo_card_link');
        Configuration::deleteByName('mts_payu_extrainfo_pse_mode');
        Configuration::deleteByName('mts_payu_extrainfo_pse_link');
        Configuration::deleteByName('mts_payu_extrainfo_efecty_mode');
        Configuration::deleteByName('mts_payu_extrainfo_efecty_link');
        Configuration::deleteByName('mts_payu_extrainfo_baloto_mode');
        Configuration::deleteByName('mts_payu_extrainfo_baloto_link');
        Configuration::deleteByName('mts_payu_extrainfo_othercash_mode');
        Configuration::deleteByName('mts_payu_extrainfo_othercash_link');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    public function installOrderState()
    {
        if (Configuration::get('PS_OS_MTS_PAYU_PENDING_CARD') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: T. Crédito/Débito')));
            $order_state->invoice = false;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#8796a5';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = false;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = false;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_CARD', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PENDING_PSE') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Pagos en Línea (PSE)')));
            $order_state->invoice = false;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#9fa587';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = false;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = false;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_PSE', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Efecty')));
            $order_state->invoice = false;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#a5a187';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = false;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = false;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_EFECTY', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PENDING_BALOTO') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Baloto')));
            $order_state->invoice = false;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#87a598';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = false;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = false;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_BALOTO', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_pending.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PAID_CARD') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: T. Crédito/Débito')));
            $order_state->invoice = true;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#007fff';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = true;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = true;
            $order_state->pdf_invoice = false;
            $order_state->pdf_delivery = false;
            $order_state->deleted = false;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PAID_CARD', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PAID_PSE') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Pagos en Línea (PSE)')));
            $order_state->invoice = true;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#ccff00';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = true;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = true;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PAID_PSE', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PAID_EFECTY') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Efecty')));
            $order_state->invoice = true;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#ffdd00';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = true;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = true;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PAID_EFECTY', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        if (Configuration::get('PS_OS_MTS_PAYU_PAID_BALOTO') < 1) {
            $order_state = new OrderState();
            $order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Baloto')));
            $order_state->invoice = true;
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->color = '#00ff90';
            $order_state->unremovable = true;
            $order_state->hidden = false;
            $order_state->logable = true;
            $order_state->delivery = false;
            $order_state->shipped = false;
            $order_state->paid = true;
            
            if ($order_state->add()) {
                Configuration::updateValue('PS_OS_MTS_PAYU_PAID_BALOTO', $order_state->id);

                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif'
                );
                copy(
                    dirname(__FILE__).'/os_paid.gif',
                    dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif'
                );
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * Declaration of variable who will contain all messages.
         */

        $messages = null;

        /**
         * If the Company Data Form was sent, confirm if all fields was filled.
         * If have errors, will be showed in the BO, else will show a confirmation message.
         */

        if (Tools::isSubmit('btn_Company_Data_Submit')) {
            $this->postValidation('company_info');

            if (!count($this->_postErrors)) {
                $this->postProcess();
                $messages .= $this->displayConfirmation($this->l('Información de la compañía actualizada'));
            } else {
                foreach ($this->_postErrors as $err) {
                    $messages .= $this->displayError($err);
                }
            }
        }

        /**
         * If the API Data Form was sent, confirm if all fields was filled.
         * If have errors, will be showed in the BO, else will show a confirmation message.
         */

        if (Tools::isSubmit('btn_API_Data_Submit')) {
            $this->postValidation('api_info');

            if (!count($this->_postErrors)) {
                $this->postProcess();
                $messages .= $this->displayConfirmation($this->l('Información de la API actualizada'));
            } else {
                foreach ($this->_postErrors as $err) {
                    $messages .= $this->displayError($err);
                }
            }
        }

        /**
         * If the Extra Info Form was sent, confirm if all fields was filled.
         * If have errors, will be showed in the BO, else will show a confirmation message.
         */

        if (Tools::isSubmit('btn_ExtraInfo_Data_Submit')) {
            $this->postValidation('extra_info');

            if (!count($this->_postErrors)) {
                $this->postProcess();
                $messages .= $this->displayConfirmation($this->l('Información adicional en los pagos actualizada'));
            } else {
                foreach ($this->_postErrors as $err) {
                    $messages .= $this->displayError($err);
                }
            }
        }

        /**
         * Set in a smarty variable, the module dir path
         */

        $this->context->smarty->assign('module_dir', $this->_path);

        /**
        * Define a variable with a template HTML content
        */

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$messages.$this->renderForm();
    }

    /**
     * This function will validate if all required fields of information have been filled out.
     */

    private function postValidation($form)
    {
        if ($form == 'api_info') {
            if (!Tools::getValue('mts_payu_api_key')) {
                $this->_postErrors[] = $this->l('La API Key es requerida.');
            }

            if (!Tools::getValue('mts_payu_api_login')) {
                $this->_postErrors[] = $this->l('El API Login es requerido.');
            }

            if (!Tools::getValue('mts_payu_api_idmerchant')) {
                $this->_postErrors[] = $this->l('El Id del Comercio es requerido.');
            }

            if (!Tools::getValue('mts_payu_api_idaccount')) {
                $this->_postErrors[] = $this->l('El Id de la Cuenta es requerido.');
            }

            if (!Tools::getValue('mts_payu_api_country') || Tools::getValue('mts_payu_api_country') == 'invalid') {
                $this->_postErrors[] = $this->l('Seleccione un país válido.');
            }

            if (!Tools::getValue('mts_payu_api_language') || Tools::getValue('mts_payu_api_language') == 'invalid') {
                $this->_postErrors[] = $this->l('Seleccione un idioma válido.');
            }
        }

        if ($form == 'company_info') {
            if (!Tools::getValue('mts_payu_company_name')) {
                $this->_postErrors[] = $this->l('El nombre o razón social de la compañia es requerida.');
            }

            if (!Tools::getValue('mts_payu_company_nit')) {
                $this->_postErrors[] = $this->l('El NIT o identificación de la compañia es requerido.');
            }
        }

        if ($form == 'extra_info') {
            if (Tools::getValue('mts_payu_extrainfo_card_mode') == true && !Tools::getValue('mts_payu_extrainfo_card_link')) {
                $this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Tarjetas Crédito/Débito');
            }

            if (Tools::getValue('mts_payu_extrainfo_pse_mode') == true && !Tools::getValue('mts_payu_extrainfo_pse_link')) {
                $this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en Línea (PSE)');
            }

            if (Tools::getValue('mts_payu_extrainfo_efecty_mode') == true && !Tools::getValue('mts_payu_extrainfo_efecty_link')) {
                $this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en efectívo (Efecty)');
            }

            if (Tools::getValue('mts_payu_extrainfo_baloto_mode') == true && !Tools::getValue('mts_payu_extrainfo_baloto_link')) {
                $this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en efectívo (Baloto)');
            }

            if (Tools::getValue('mts_payu_extrainfo_othercash_mode') == true && !Tools::getValue('mts_payu_extrainfo_othercash_link')) {
                $this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en efectívo (Otros medios)');
            }
        }
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitMtspayuapiModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm($this->getConfigForm());
    }

    /**
     * Create the structure(s) of your form(s).
     */
    protected function getConfigForm()
    {
        $optionCountries = array(
            'query' => array(
                0 => array(
                    'id_country' => 'invalid',
                    'name_country' => '--'
                ),
                1 => array(
                    'id_country' => 'AR',
                    'name_country' => 'Argentina'
                ),
                2 => array(
                    'id_country' => 'BR',
                    'name_country' => 'Brasil'
                ),
                3 => array(
                    'id_country' => 'CO',
                    'name_country' => 'Colombia'
                ),
                4 => array(
                    'id_country' => 'MX',
                    'name_country' => 'México'
                ),
                5 => array(
                    'id_country' => 'PA',
                    'name_country' => 'Panamá'
                ),
                6 => array(
                    'id_country' => 'PE',
                    'name_country' => 'Perú'
                )
            ),
            'id' => 'id_country',
            'name' => 'name_country'
        );

        $optionLanguages = array(
            'query' => array(
                0 => array(
                    'id_language' => 'invalid',
                    'name_language' => '--'
                ),
                1 => array(
                    'id_language' => 'es',
                    'name_language' => 'Español'
                ),
                2 => array(
                    'id_language' => 'en',
                    'name_language' => 'Inglés'
                ),
                3 => array(
                    'id_language' => 'pt',
                    'name_language' => 'Portugués'
                )
            ),
            'id' => 'id_language',
            'name' => 'name_language'
        );

        $company_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Datos de la Compañia'),
                    'icon' => 'icon-briefcase'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('En pruebas'),
                        'desc' => $this->l('Si habilita, la API funcionará en modo de pruebas'),
                        'name' => 'mts_payu_sandbox_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Razón social'),
                        'desc' => $this->l('Ingrese la razón social o el nombre de la compañía'),
                        'name' => 'mts_payu_company_name',
                        'required' => false
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('NIT'),
                        'desc' => $this->l('Ingrese el NIT o identificación de la compañía'),
                        'name' => 'mts_payu_company_nit',
                        'required' => true
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Guardar'),
                    'name' => 'btn_Company_Data_Submit'
                )
            ),
        );

        $api_data_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Datos de la API de PayU'),
                    'icon' => 'icon-key'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('API Key'),
                        'desc' => $this->l('Ingrese el API Key que le fue proporcionado por PayU'),
                        'name' => 'mts_payu_api_key',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API Login'),
                        'desc' => $this->l('Ingrese el API Login que le fue proporcionado por PayU'),
                        'name' => 'mts_payu_api_login',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Llave pública'),
                        'desc' => $this->l('Ingrese la Llave Pública que le fue proporcionado por PayU'),
                        'name' => 'mts_payu_api_publickey',
                        'required' => false
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('ID del comercio'),
                        'desc' => $this->l('Ingrese el ID de Comercio que le fue proporcionado por PayU'),
                        'name' => 'mts_payu_api_idmerchant',
                        'required' => true
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('ID de la cuenta'),
                        'desc' => $this->l('Ingrese el ID de la Cuenta que le fue proporcionado por PayU'),
                        'name' => 'mts_payu_api_idaccount',
                        'required' => true
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('País'),
                        'desc' => $this->l('Seleccione el país registrado para esta cuenta de PayU.'),
                        'name' => 'mts_payu_api_country',
                        'required' => true,
                        'options' => $optionCountries
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Idioma'),
                        'desc' => $this->l('Seleccione el idioma que desea utilizar para la API.'),
                        'name' => 'mts_payu_api_language',
                        'required' => true,
                        'options' => $optionLanguages
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Guardar'),
                    'name' => 'btn_API_Data_Submit'
                )
            ),
        );

        $info_extra_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Información adicional en los métodos de págo'),
                    'icon' => 'icon-link'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Mostrar en Tarjetas Crédito/Débito'),
                        'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
                        'name' => 'mts_payu_extrainfo_card_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Enlace para Tarjetas Crédito/Débito'),
                        'desc' => $this->l('Ingrese el enlace a la información adicional para Tarjetas Crédito/Débito'),
                        'name' => 'mts_payu_extrainfo_card_link',
                        'required' => false
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Mostrar en Pagos en Línea (PSE)'),
                        'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
                        'name' => 'mts_payu_extrainfo_pse_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Enlace para Pagos en Línea (PSE)'),
                        'desc' => $this->l('Ingrese el enlace a la información adicional para Pagos en Línea (PSE)'),
                        'name' => 'mts_payu_extrainfo_pse_link',
                        'required' => false
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Mostrar en Efecty'),
                        'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
                        'name' => 'mts_payu_extrainfo_efecty_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Enlace para Efecty'),
                        'desc' => $this->l('Ingrese el enlace a la información adicional para Efecty'),
                        'name' => 'mts_payu_extrainfo_efecty_link',
                        'required' => false
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Mostrar en Baloto'),
                        'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
                        'name' => 'mts_payu_extrainfo_baloto_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Enlace para Baloto'),
                        'desc' => $this->l('Ingrese el enlace a la información adicional para Baloto'),
                        'name' => 'mts_payu_extrainfo_baloto_link',
                        'required' => false
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Mostrar en Otros medios'),
                        'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
                        'name' => 'mts_payu_extrainfo_othercash_mode',
                        'required' => false,
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Si')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            ),
                        ),
                        
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Enlace para Otros medios'),
                        'desc' => $this->l('Ingrese el enlace a la información adicional para Otros medios'),
                        'name' => 'mts_payu_extrainfo_othercash_link',
                        'required' => false
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Guardar'),
                    'name' => 'btn_ExtraInfo_Data_Submit'
                )
            ),
        );

        return array($company_form, $api_data_form, $info_extra_form);
    }


    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        $config = Configuration::getMultiple(
            array(
                'mts_payu_api_key',
                'mts_payu_api_login',
                'mts_payu_api_publickey',
                'mts_payu_api_idmerchant',
                'mts_payu_api_idaccount',
                'mts_payu_api_country',
                'mts_payu_api_language',
                'mts_payu_sandbox_mode',
                'mts_payu_company_name',
                'mts_payu_company_nit',
                'mts_payu_extrainfo_card_mode',
                'mts_payu_extrainfo_card_link',
                'mts_payu_extrainfo_pse_mode',
                'mts_payu_extrainfo_pse_link',
                'mts_payu_extrainfo_efecty_mode',
                'mts_payu_extrainfo_efecty_link',
                'mts_payu_extrainfo_baloto_mode',
                'mts_payu_extrainfo_baloto_link',
                'mts_payu_extrainfo_othercash_mode',
                'mts_payu_extrainfo_othercash_link'
            )
        );

        return $config;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name || Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     * NOT WORKING!!
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/front.js');
        $this->context->controller->addCSS($this->_path.'views/css/front.css');
    }

    /**
     * This method is used to render the payment button,
     * Take care if the button should be displayed or not.
     */
    public function hookPayment($params)
    {
        $currency_id = $params['cart']->id_currency;
        $currency = new Currency((int)$currency_id);

        if (in_array($currency->iso_code, $this->limited_currencies) == false) {
            return false;
        }

        $this->smarty->assign('module_dir', $this->_path);

        return $this->display(__FILE__, 'views/templates/hook/payment.tpl');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        if ($this->active == false) {
            return;
        }

        $order = $params['objOrder'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR')) {
            $this->smarty->assign('status', 'ok');
        }

        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    /**
     * This hook is used to set new payment methods in the checkout.
     */

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        if (!$this->checkCurrency($params['cart'])) {
            return;
        }

        $parameters = array(
            'deviceSessionId' => md5(session_id().microtime()),
            'currency' => $this->context->currency,
        );

        $payment_options = array(
            $this->getCreditCardOption($parameters),
            $this->getPSEOption($parameters),
            $this->getBalotoOption($parameters),
            $this->getEfectyOption($parameters),
            $this->getOtherCashOption($parameters),
        );

        return $payment_options;
    }

    /**
     * This function check the currency.
     */

    public function checkCurrency($cart)
    {
        $currency_order = new Currency((int)($cart->id_currency));
        $currencies_module = $this->getCurrency((int)$cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This function setup the Credit Card payment option.
     */

    public function getCreditCardOption($parameters)
    {
        $parameters['type'] = 'card';

        $creditCardOption = new PaymentOption();
        $creditCardOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pago con PayU: Tarjeta de Crédito'))
                ->setForm($this->generatePaymentForm($parameters))
                ->setAction($this->context->link->getModuleLink($this->name, 'validationCard', array(), true))
                ->setAdditionalInformation($this->fetch('module:mtspayuapi/views/templates/front/payment_infos/creditcard.tpl'));

        return $creditCardOption;
    }

    /**
     * This function setup the PSE payment option.
     */
    
    public function getPSEOption($parameters)
    {
        $parameters['type'] = 'pse';

        $pseOption = new PaymentOption();
        $pseOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pago con PayU: Transferencia Bancaria (PSE)'))
                ->setForm($this->generatePaymentForm($parameters))
                ->setAction($this->context->link->getModuleLink($this->name, 'validationPse', array(), true))
                ->setAdditionalInformation($this->fetch('module:mtspayuapi/views/templates/front/payment_infos/pse.tpl'));

        return $pseOption;
    }

    /**
     * This function setup the Baloto payment option.
     */
    
    public function getBalotoOption($parameters)
    {
        $parameters['type'] = 'baloto';

        $balotoOption = new PaymentOption();
        $balotoOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pago con PayU: Baloto'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validationBaloto', array(), true))
                ->setAdditionalInformation($this->fetch('module:mtspayuapi/views/templates/front/payment_infos/baloto.tpl'));

        return $balotoOption;
    }

    /**
     * This function setup the Efecty payment option.
     */
    
    public function getEfectyOption($parameters)
    {
        $parameters['type'] = 'efecty';

        $efectyOption = new PaymentOption();
        $efectyOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pago con PayU: Efecty'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validationEfecty', array(), true))
                ->setAdditionalInformation($this->fetch('module:mtspayuapi/views/templates/front/payment_infos/efecty.tpl'));

        return $efectyOption;
    }

    /**
     * This function setup other cash payments options.
     */
    
    public function getOtherCashOption($parameters)
    {
        $parameters['type'] = 'othercash';
        
        $otherCashOption = new PaymentOption();
        $otherCashOption->setModuleName($this->name)
                ->setCallToActionText($this->l('Pago con PayU: Otros puntos'))
                ->setAction($this->context->link->getModuleLink($this->name, 'validationOtherCash', array(), true))
                ->setAdditionalInformation($this->fetch('module:mtspayuapi/views/templates/front/payment_infos/othercash.tpl'));

        return $otherCashOption;
    }

    /**
     * This function generate the Credit Card and PSE payments options forms.
     */

    protected function generatePaymentForm($parameters)
    {

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('deviceSessionId', $parameters['deviceSessionId']);
        $this->context->smarty->assign('type', $parameters['type']);


        if ($parameters['type'] == 'card') {
            $months = array();
            for ($i = 1; $i <= 12; $i++) {
                $months[] = sprintf("%02d", $i);
            }
            $years = array();
            for ($i = 0; $i <= 10; $i++) {
                $years[] = date('Y', strtotime('+'.$i.' years'));
            }
            $this->context->smarty->assign(array(
                'action' => $this->context->link->getModuleLink($this->name, 'validationCard', array(), true),
                'months' => $months,
                'years' => $years,
            ));

            return $this->context->smarty->fetch('module:mtspayuapi/views/templates/front/payment_forms/creditcard.tpl');
        }

        if ($parameters['type'] == 'pse') {
            $months = array();
            for ($i = 1; $i <= 12; $i++) {
                $months[] = sprintf("%02d", $i);
            }
            $years = array();
            for ($i = 0; $i <= 10; $i++) {
                $years[] = date('Y', strtotime('+'.$i.' years'));
            }
            $this->context->smarty->assign(array(
                'action' => $this->context->link->getModuleLink($this->name, 'validationPse', array(), true),
                'months' => $months,
                'years' => $years,
            ));

            return $this->context->smarty->fetch('module:mtspayuapi/views/templates/front/payment_forms/pse.tpl');
        }
    }
    

    public function hookActionPaymentCCAdd()
    {
        /* Place your code here. */
    }

    public function hookActionPaymentConfirmation()
    {
        /* Place your code here. */
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     * Only applies if the page is the same to 'order'
     */

    public function hookDisplayHeader()
    {
        if ('order' === $this->context->controller->php_self) {
            $this->context->controller->registerStylesheet('modules-front', 'modules/'.$this->name.'/views/css/front.css', array('media' => 'all', 'priority' => 150));
            // $this->context->controller->registerStylesheet('modules-front', 'modules/'.$this->name.'/views/css/jquery.fancybox-1.3.4.css', ['media' => 'all', 'priority' => 150]);
            $this->context->controller->registerJavascript('modules-front', 'modules/'.$this->name.'/views/js/front.js', array('position' => 'bottom', 'priority' => 150));
            // $this->context->controller->registerJavascript('modules-front', 'modules/'.$this->name.'/views/js/jquery.fancybox-1.3.4.pack.js', ['position' => 'bottom', 'priority' => 150]);
        }
    }

    public function hookDisplayPayment()
    {
        /* Place your code here. */
    }

    public function hookDisplayPaymentReturn()
    {
        /* Place your code here. */
    }
}
